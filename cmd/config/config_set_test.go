package config

import (
	"testing"
)

func TestPropertyInConfig(t *testing.T) {
	list := []string{"a", "b"}

	resTrue := propertyInConfig("a", list)

	if resTrue != true {
		t.Error("Expected property to be in config")
	}

	resFalse := propertyInConfig("c", list)

	if resFalse != false {
		t.Error("Expected property to not be in config")
	}
}

func TestNewCmdConfigSet(t *testing.T) {
	cmd := NewCmdConfigSet()

	if cmd.Use != "set PROPERTY_NAME PROPERTY_VALUE" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}
