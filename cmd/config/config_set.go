package config

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func propertyInConfig(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// NewCmdConfigSet docs
func NewCmdConfigSet() *cobra.Command {
	return &cobra.Command{
		Use:   "set PROPERTY_NAME PROPERTY_VALUE",
		Short: "Set a config property",
		Long:  `set a config property`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}
			if propertyInConfig(args[0], viper.AllKeys()) {
				viper.Set(args[0], args[1])
				viper.WriteConfig()
				fmt.Printf("config updated. %s set to %s\n", args[0], args[1])
			} else {
				fmt.Printf("%s is not a valid property name\n", args[0])
			}
		},
	}
}
