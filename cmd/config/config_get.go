package config

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// NewCmdConfigGet docs
func NewCmdConfigGet() *cobra.Command {
	return &cobra.Command{
		Use:   "get PROPERTY_NAME",
		Short: "Get a config property",
		Long:  `Get a config property`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}
			if propertyInConfig(args[0], viper.AllKeys()) {
				fmt.Println(viper.Get(args[0]))
			} else {
				fmt.Printf("%s is not a valid property name\n", args[0])
			}
		},
	}
}
