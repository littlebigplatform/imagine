package config

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdConfig docs
func NewCmdConfig() *cobra.Command {
	c := &cobra.Command{
		Use:   "config [command]",
		Short: "Configure commands",
		Long:  `configure commands`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdConfigSet())
	c.AddCommand(NewCmdConfigGet())

	return c
}
