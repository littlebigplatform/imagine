package config

import (
	"testing"
)

func TestNewCmdConfig(t *testing.T) {
	cmd := NewCmdConfig()

	if cmd.Use != "config [command]" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 2 {
		t.Errorf("wrong number of subcommands")
	}
}
