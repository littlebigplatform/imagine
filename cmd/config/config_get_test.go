package config

import (
	"testing"
)

func TestNewCmdConfigGet(t *testing.T) {
	cmd := NewCmdConfigGet()

	if cmd.Use != "get PROPERTY_NAME" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}
