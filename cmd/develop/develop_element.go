package develop

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	// "github.com/spf13/viper"
)

func developElement() {
	fmt.Println("Developing elements")
}

// NewCmdDevelopElement docs
func NewCmdDevelopElement() *cobra.Command {
	return &cobra.Command{
		Use:   "element",
		Short: "move to elements directory",
		Long:  `move to elements directory`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			developElement()
		},
	}
}
