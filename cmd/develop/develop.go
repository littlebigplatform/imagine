package develop

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdDevelop docs
func NewCmdDevelop() *cobra.Command {
	c := &cobra.Command{
		Use:   "develop ITEM",
		Short: "develop elements and compounds",
		Long:  `develop elements and compounds`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdDevelopElement())

	return c
}
