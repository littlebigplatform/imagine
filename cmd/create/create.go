package create

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdCreate docs
func NewCmdCreate() *cobra.Command {
	c := &cobra.Command{
		Use:   "create [command]",
		Short: "Perform a create operation",
		Long:  `create a thing`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdCreateElement())
	c.AddCommand(NewCmdCreateCompound())
	c.AddCommand(NewCmdCreateManifest())

	return c
}
