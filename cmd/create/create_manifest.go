package create

import (
	"fmt"
	"os"
	"path"

	"gitlab.com/littlebigplatform/imagine/git"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func createManifest(name string) {
	path := path.Join(viper.GetString("paths.manifests"), name)
	opts := &git.NewRepositoryOptions{
		Path:         path,
		RemoteType:   viper.GetString("remote.type"),
		Organization: viper.GetString("remote.organization"),
		PAT:          viper.GetString("remote.pat"),
		Name:         name,
		User: git.User{
			Name:  viper.GetString("remote.user.name"),
			Email: viper.GetString("remote.user.email"),
		},
	}
	r := git.NewRepository(opts)

	if r.Remote().Exists(name) {
		fmt.Printf("%s already exists in remote\n", name)
		os.Exit(0)
	}

	err := r.Remote().Create(name)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	tURL := r.Remote().GetRepoURL("manifest-template")
	r.Clone(tURL)

	tURL = r.Remote().GetRepoURL(name)
	r.Reset(tURL)
}

// NewCmdCreateManifest doc
func NewCmdCreateManifest() *cobra.Command {
	return &cobra.Command{
		Use:   "manifest MANIFEST_NAME",
		Short: "Create a manifest",
		Long:  `create a manifest`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			createManifest(args[0])
		},
	}
}
