package create

import (
	"testing"
)

func TestNewCmdCreateCompound(t *testing.T) {
	cmd := NewCmdCreateCompound()

	if cmd.Use != "compound COMPOUND_NAME" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}
