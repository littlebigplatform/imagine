package create

import (
	"fmt"
	"os"
	"path"

	"gitlab.com/littlebigplatform/imagine/git"

	"github.com/otiai10/copy"
)

type createOptions struct {
	Path         string
	Name         string
	Type         string
	Organization string
	RemoteType   string
	Username     string
	Email        string
	PAT          string
}

func cloneOrPull(r git.IRepository, t string) {
	exists := r.Remote().Exists(t)

	if !exists {
		fmt.Println(t + " remote does not exist")
		os.Exit(1)
	}

	if _, err := os.Stat(path.Join(r.Path(), ".git")); os.IsNotExist(err) {
		tURL := r.Remote().GetRepoURL(t)
		r.Clone(tURL)
	} else {
		r.CheckoutMaster()
	}
}

func create(createOptions *createOptions) {
	opts := &git.NewRepositoryOptions{
		Path:         createOptions.Path,
		RemoteType:   createOptions.RemoteType,
		Organization: createOptions.Organization,
		PAT:          createOptions.PAT,
		Name:         createOptions.Name,
		User: git.User{
			Name:  createOptions.Username,
			Email: createOptions.Email,
		},
	}
	r := git.NewRepository(opts)

	cloneOrPull(r, createOptions.Type)
	r.CreateBranch(createOptions.Name)
	copy.Copy(path.Join(r.Path(), "template"), path.Join(r.Path(), createOptions.Name))
}
