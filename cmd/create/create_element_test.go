package create

import (
	"testing"
)

func TestNewCmdCreateElement(t *testing.T) {
	cmd := NewCmdCreateElement()

	if cmd.Use != "element ELEMENT_NAME" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}
