package create

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func createElement(name string) {
	create(&createOptions{
		Path:         viper.GetString("paths.elements"),
		Name:         name,
		Type:         "elements",
		Organization: viper.GetString("remote.organization"),
		RemoteType:   viper.GetString("remote.type"),
		Username:     viper.GetString("remote.user.name"),
		Email:        viper.GetString("remote.user.email"),
		PAT:          viper.GetString("remote.pat"),
	})
}

// NewCmdCreateElement docs
func NewCmdCreateElement() *cobra.Command {
	return &cobra.Command{
		Use:   "element ELEMENT_NAME",
		Short: "Create an element",
		Long:  `create an element`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			createElement(args[0])
		},
	}
}
