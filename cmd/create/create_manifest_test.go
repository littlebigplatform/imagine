package create

import (
	"testing"
)

func TestNewCmdCreateManifest(t *testing.T) {
	cmd := NewCmdCreateManifest()

	if cmd.Use != "manifest MANIFEST_NAME" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}
