package create

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

func createCompound(name string) {
	create(&createOptions{
		Path:         viper.GetString("paths.compounds"),
		Name:         name,
		Type:         "compounds",
		Organization: viper.GetString("remote.organization"),
		RemoteType:   viper.GetString("remote.type"),
		Username:     viper.GetString("remote.user.name"),
		Email:        viper.GetString("remote.user.email"),
	})
}

// NewCmdCreateCompound doc
func NewCmdCreateCompound() *cobra.Command {
	return &cobra.Command{
		Use:   "compound COMPOUND_NAME",
		Short: "Create a compound",
		Long:  `create a compound`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			createCompound(args[0])
		},
	}
}
