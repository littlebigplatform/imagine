package create

import (
	"testing"
)

func TestNewCmdCreate(t *testing.T) {
	cmd := NewCmdCreate()

	if cmd.Use != "create [command]" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 3 {
		t.Errorf("wrong number of subcommands")
	}
}
