package imagine

import (
	"testing"
)

func TestNewCmdImagine(t *testing.T) {
	cmd := NewCmdImagine("dev", "abcd", "unknown")

	if cmd.Use != "imagine" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 8 {
		t.Errorf("wrong number of subcommands")
	}

}
