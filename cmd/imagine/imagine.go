package imagine

import (
	"fmt"
	"os"
	"path"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"gitlab.com/littlebigplatform/imagine/cmd/config"
	"gitlab.com/littlebigplatform/imagine/cmd/configs"
	"gitlab.com/littlebigplatform/imagine/cmd/create"
	"gitlab.com/littlebigplatform/imagine/cmd/develop"
	"gitlab.com/littlebigplatform/imagine/cmd/list"
	"gitlab.com/littlebigplatform/imagine/cmd/promote"
	"gitlab.com/littlebigplatform/imagine/cmd/publish"
	lbpversion "gitlab.com/littlebigplatform/imagine/cmd/version"
)

var cfgFile string

// NewCmdImagine Comment to please linter
func NewCmdImagine(version string, commit string, date string) *cobra.Command {
	cobra.OnInitialize(initConfig)
	rootCmd := &cobra.Command{
		Use:                   "imagine",
		Version:               version,
		DisableFlagsInUseLine: true,
		Short:                 "imagine is the tool for Little Big Platform",
		Long:                  `imagine provides an interface to your Platform`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}
		},
	}

	rootCmd.AddCommand(create.NewCmdCreate())
	rootCmd.AddCommand(config.NewCmdConfig())
	rootCmd.AddCommand(configs.NewCmdConfigs())
	rootCmd.AddCommand(develop.NewCmdDevelop())
	rootCmd.AddCommand(promote.NewCmdPromote())
	rootCmd.AddCommand(publish.NewCmdPublish())
	rootCmd.AddCommand(list.NewCmdList())
	rootCmd.AddCommand(lbpversion.NewCmdVersion(&lbpversion.Version{
		Version: version,
		Commit:  commit,
		Date:    date,
	}))

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config-file", "f", "", "config file (default is $HOME/.imagine)")

	return rootCmd
}

func initConfig() {
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	viper.SetDefault("paths.elements", path.Join(home, "lbp", "elements"))
	viper.SetDefault("paths.compounds", path.Join(home, "lbp", "compounds"))
	viper.SetDefault("paths.manifests", path.Join(home, "lbp", "manifests"))
	viper.SetDefault("remote.organization", "littlebigplatform")
	viper.SetDefault("remote.type", "gitlab")
	viper.SetDefault("remote.user.name", "")
	viper.SetDefault("remote.user.email", "")
	viper.SetDefault("remote.pat", "")

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.SetConfigType("toml")
		viper.SetConfigName(".imagine")
		viper.AddConfigPath(home)
	}

	err = viper.ReadInConfig()

	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			if cfgFile != "" {
				fmt.Println(err)
				os.Exit(1)
			}
			os.Create(path.Join(home, ".imagine.toml"))
		} else {
			fmt.Println("Can't read config:", err)
			os.Exit(1)
		}
	}
}
