package publish

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strconv"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/littlebigplatform/imagine/docker"
	"gitlab.com/littlebigplatform/imagine/git"
	"golang.org/x/net/context"
	"gopkg.in/yaml.v2"
)

type Artifact struct {
	Path              string
	Tag               string
	Version           string   `yaml:"version"`
	Type              string   `yaml:"type"`
	Image             string   `yaml:"image"`
	TestCmds          []string `yaml:"test"`
	DeployCmds        []string `yaml:"deploy"`
	Elements          []string `yaml:"elements"`
	RequiredVariables []string `yaml:"required_variables"`
}

type ArtifactDefault struct {
	Type     string   `yaml:"type"`
	Image    string   `yaml:"image"`
	TestCmds []string `yaml:"test"`
}

func getArtifactType(artifactsPath string) (string, error) {
	m := ArtifactDefault{}
	imPath := path.Join(artifactsPath, "imagine.yml")
	dat, err := ioutil.ReadFile(imPath)
	if err != nil {
		return "", err
	}

	err = yaml.Unmarshal([]byte(dat), &m)
	if err != nil {
		return "", err
	}
	return m.Type, nil
}

func getArtifactDefaults(artifactsPath string) (map[string]ArtifactDefault, error) {
	m := make(map[string]ArtifactDefault)
	imPath := path.Join(artifactsPath, "imagine.yml")
	dat, _ := ioutil.ReadFile(imPath)
	err := yaml.Unmarshal([]byte(dat), &m)
	if err != nil {
		return nil, err
	}
	return m, nil
}

func getChangedArtifacts(artifactsPath string, changedDirs []string) ([]*Artifact, error) {
	artifacts := make([]*Artifact, len(changedDirs))
	for i, dir := range changedDirs {
		imPath := path.Join(artifactsPath, dir, "imagine.yml")
		dat, _ := ioutil.ReadFile(imPath)
		a := &Artifact{
			Path: path.Join(artifactsPath, dir),
			Tag:  dir,
		}
		err := yaml.Unmarshal([]byte(dat), a)
		if err != nil {
			return nil, err
		}
		artifacts[i] = a
	}
	artDef, _ := getArtifactDefaults(artifactsPath)
	for _, art := range artifacts {
		if art.Image == "" {
			art.Image = artDef[art.Type].Image
		}
		if len(art.TestCmds) == 0 {
			art.TestCmds = artDef[art.Type].TestCmds
		}
	}
	return artifacts, nil
}

func (a Artifact) Test() {
	for _, cmd := range a.TestCmds {
		c := strings.Split(cmd, " ")
		res, err := docker.Run(context.Background(), &docker.RunOptions{
			Image:      a.Image,
			Datadir:    a.Path,
			Cmd:        c,
			AutoRemove: true,
		})
		if err != nil {
			panic(err)
		}

		fmt.Printf("%s\n", res.StdOut)
		fmt.Printf("%s\n", res.StdErr)
	}
}

func getDiffRefs(r git.IRepository) (string, string) {
	cb, err := r.BranchName()
	if err != nil {
		fmt.Println("getDiffRefs:CurrentBranchError")
		fmt.Println(err)
	}

	lt, err := r.LatestTag()
	if err != nil {
		fmt.Println("getDiffRefs:LatestTagError")
		fmt.Println(err)
	}
	if lt == "" {
		lt = r.InitalCommitHash()
	}

	return cb, lt
}

func changedFiles(r git.IRepository, latestTagOrHash string) []string {
	fileschanged := make([]string, 0)
	diff, _ := r.Diff(latestTagOrHash)
	stats := diff.Stats()

	for _, stat := range stats {
		fileschanged = append(fileschanged, stat.Name)
	}

	fps := diff.FilePatches()
	for _, fp := range fps {
		f, t := fp.Files()
		if t == nil {
			fileschanged = removeFromSlice(fileschanged, f.Path())
		}
	}

	return fileschanged
}

func changedDirs(filesChanged []string) []string {
	dirschanged := make([]string, 0)
	for _, fc := range filesChanged {
		a := strings.Split(fc, "/")
		if len(a) > 2 { // make sure the changed file is of the form [azure resource-group main.tf]
			inDirschanged := false
			for _, dir := range dirschanged {
				if dir == strings.Join(a[0:2], "/") {
					inDirschanged = true
				}
			}
			if inDirschanged == false {
				dirschanged = append(dirschanged, strings.Join(a[0:2], "/"))
			}
		}
	}
	return dirschanged
}

func newTag(r git.IRepository, tag string, version string) string {
	patchVersion := 0

	ltc, _ := r.LatestTagContains(tag)
	if ltc != "" {
		r := "_v"
		if tag == "" {
			r = "v"
		}
		lastVersion := regexp.MustCompile(r).Split(ltc, 2)[1]
		latestPatch, _ := strconv.Atoi(strings.Split(lastVersion, ".")[2])
		patchVersion = latestPatch + 1
	}

	v := "v" + version + "." + strconv.Itoa(patchVersion)

	if tag == "" {
		return v
	}

	return tag + "_" + v
}

func removeFromSlice(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func tagManifests(opts *git.NewRepositoryOptions, currentBranch string, latestTag string, remoteURL string) {
	m := Artifact{}
	imPath := path.Join(opts.Path, "imagine.yml")
	dat, err := ioutil.ReadFile(imPath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	err = yaml.Unmarshal([]byte(dat), &m)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	r := git.NewRepository(opts)
	nt := newTag(r, "", m.Version)
	fmt.Printf("new version: %s\n", nt)
	if strings.Contains(currentBranch, "master") {
		fmt.Printf("tagging: %s\n", nt)
		r.Tag(nt)
	}
	filesChanged := changedFiles(r, latestTag)

	if strings.Contains(currentBranch, "master") && len(filesChanged) > 0 {
		r.AddRemote("ci", remoteURL)
		err := r.PushWithTagsTo("ci")
		if err != nil {
			fmt.Println("Error pushing to remote")
			fmt.Println(err)
			os.Exit(1)
		}
	}
}

func publishArtifacts(diffOnly bool, opts *git.NewRepositoryOptions, remoteURL string, test bool) {
	r := git.NewRepository(opts)

	cb, lt := getDiffRefs(r)
	fmt.Printf("Current Branch: %s\n", cb)
	fmt.Printf("Latest Tag: %s\n", lt)

	at, _ := getArtifactType(opts.Path)

	if at == "manifests" {
		tagManifests(opts, cb, lt, remoteURL)
	} else {
		filesChanged := changedFiles(r, lt)
		dirsChanged := changedDirs(filesChanged)
		arts, _ := getChangedArtifacts(opts.Path, dirsChanged)

		if diffOnly {
			for _, a := range arts {
				fmt.Println(a.Path)
				if test {
					a.Test()
				}
			}
			os.Exit(0)
		}

		for _, a := range arts {
			nt := newTag(r, a.Tag, a.Version)
			fmt.Printf("new version: %s\n", nt)
			if strings.Contains(cb, "master") {
				fmt.Printf("tagging: %s\n", nt)
				r.Tag(nt)
			}
		}

		if strings.Contains(cb, "master") && len(dirsChanged) > 0 {
			r.AddRemote("ci", remoteURL)
			err := r.PushWithTagsTo("ci")
			if err != nil {
				fmt.Println("Error pushing to remote")
				fmt.Println(err)
				os.Exit(1)
			}
		}
	}

}

// NewCmdPublish docs
func NewCmdPublish() *cobra.Command {
	var (
		diffOnly    bool
		test        bool
		RemoteURL   string
		PAT         string
		GitUsername string
		GitEmail    string
	)

	c := &cobra.Command{
		Use:   "publish",
		Short: "publish artifacts",
		Long:  `Publish artifacts in the git repository.`,
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			opts := &git.NewRepositoryOptions{
				Path:         args[0],
				RemoteType:   "",
				Organization: "",
				PAT:          PAT,
				Name:         "elements",
				User: git.User{
					Name:  GitUsername,
					Email: GitEmail,
				},
			}
			publishArtifacts(diffOnly, opts, RemoteURL, test)
		},
	}

	c.Flags().BoolVarP(&diffOnly, "diff-only", "", false, "return only the elements that have changed.")
	c.Flags().BoolVarP(&test, "test", "", false, "run test commands")
	c.Flags().StringVarP(&RemoteURL, "remote-url", "", "", "path to elements local repository.")
	c.Flags().StringVarP(&PAT, "pat", "", "", "path to elements local repository.")
	c.Flags().StringVarP(&GitUsername, "git-username", "", "", "path to elements local repository.")
	c.Flags().StringVarP(&GitEmail, "git-email", "", "", "path to elements local repository.")
	c.MarkFlagRequired("remote-url")
	c.MarkFlagRequired("pat")
	c.MarkFlagRequired("git-username")
	c.MarkFlagRequired("git-email")

	return c
}
