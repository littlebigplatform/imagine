package publish

import (
	"github.com/golang/mock/gomock"
	"gitlab.com/littlebigplatform/imagine/git/mock_git"
	"testing"
)

func TestNewCmdPublish(t *testing.T) {
	cmd := NewCmdPublish()

	if cmd.Use != "publish" {
		t.Errorf("Use is not correct")
	}

	if len(cmd.Commands()) != 0 {
		t.Errorf("wrong number of subcommands")
	}
}

func TestGetDiffRefsReturnsTag(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGit := mock_git.NewMockIRepository(ctrl)
	mockGit.EXPECT().BranchName().Return("master", nil)
	mockGit.EXPECT().LatestTag().Return("tag", nil)

	cb, lt := getDiffRefs(mockGit)

	if cb != "master" {
		t.Errorf("cb: got %s, want master", cb)
	}

	if lt != "tag" {
		t.Errorf("lt: got %s, want tag", lt)
	}
}

func TestGetDiffRefsReturnsInitialHash(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockGit := mock_git.NewMockIRepository(ctrl)
	mockGit.EXPECT().BranchName().Return("master", nil)
	mockGit.EXPECT().LatestTag().Return("", nil)
	mockGit.EXPECT().InitalCommitHash().Return("123456")

	cb, lt := getDiffRefs(mockGit)

	if cb != "master" {
		t.Errorf("cb: got %s, want master", cb)
	}

	if lt != "123456" {
		t.Errorf("lt: got %s, want 123456", lt)
	}
}

func TestChangedDirs(t *testing.T) {
	fc := []string{
		"azure/resource-group/main.tf",
		"azure/resource-group/variables.tf",
		"templates/main.tf",
	}

	cd := changedDirs(fc)

	if len(cd) != 1 {
		t.Errorf("len: got %d, want 1", len(cd))
	}

	if cd[0] != "azure/resource-group" {
		t.Errorf("cd[0]: got %s, want azure/resource-group", cd[0])
	}
}

func TestRemoveFromSlice(t *testing.T) {
	sl := []string{"a", "b"}
	sl = removeFromSlice(sl, "b")

	if len(sl) != 1 {
		t.Errorf("len is %d expected 1", len(sl))

	}
}
