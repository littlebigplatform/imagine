package promote

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdManifest docs
func NewCmdPromote() *cobra.Command {
	c := &cobra.Command{
		Use:   "promote",
		Short: "promote artifacts and manifests",
		Long:  `do promotion things`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdPromoteManifest())

	return c
}
