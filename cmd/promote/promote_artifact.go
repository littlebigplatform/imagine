package promote

import (
	"fmt"
	"os"
	"strings"
	// "time"

	"github.com/spf13/cobra"
	"gitlab.com/littlebigplatform/imagine/git"
	// "github.com/spf13/viper"
)

type artifactPromoteOpts struct {
	path         string
	remoteType   string
	organization string
	pat          string
	userName     string
	userEmail    string
	targetURL    string
	targetName   string
}

func artifactPromote(opts artifactPromoteOpts) {
	fmt.Println("Promote manifest to " + opts.targetURL)

	repoOpts := &git.NewRepositoryOptions{
		Path:         opts.path,
		RemoteType:   opts.remoteType,
		Organization: opts.organization,
		PAT:          opts.pat,
		Name:         opts.targetName,
		User: git.User{
			Name:  opts.userName,
			Email: opts.userEmail,
		},
	}

	r := git.NewRepository(repoOpts)

	r.Clone(opts.targetURL)
	// r.CreateBranch("promote-" + time.Unix())
}

// NewCmdPromoteManifest docs
func NewCmdPromoteArtifact() *cobra.Command {
	var (
		targetURL   string
		pat         string
		remoteType  string
		gitUsername string
		gitEmail    string
	)

	c := &cobra.Command{
		Use:   "manifest TARGET",
		Short: "promote a manifest",
		Long:  `promote a manifest`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			path, err := os.Getwd()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}

			if remoteType == "" {
				remoteType = strings.Split(targetURL, ".com")[0]
			}

			org := strings.Split(targetURL, "/")[1]
			name := strings.Split(targetURL, "/")[2]

			opts := artifactPromoteOpts{
				path:         path,
				organization: org,
				pat:          pat,
				targetURL:    targetURL,
				remoteType:   remoteType,
				userName:     gitUsername,
				userEmail:    gitEmail,
				targetName:   name,
			}

			artifactPromote(opts)
		},
	}

	c.Flags().StringVarP(&targetURL, "target-url", "", "", "url to manifest")
	c.Flags().StringVarP(&pat, "pat", "", "", "path to elements local repository.")
	c.Flags().StringVarP(&remoteType, "remote-type", "", "", "type of remote (gitlab/github)")
	c.Flags().StringVarP(&gitUsername, "git-username", "", "", "path to elements local repository.")
	c.Flags().StringVarP(&gitEmail, "git-email", "", "", "path to elements local repository.")

	return c
}
