package promote

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	// "github.com/spf13/viper"
)

func manifestPromote(target string) {
	fmt.Println("Promote manifest to " + target)
}

// NewCmdPromoteManifest docs
func NewCmdPromoteManifest() *cobra.Command {
	return &cobra.Command{
		Use:   "manifest TARGET",
		Short: "promote a manifest",
		Long:  `promote a manifest`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			manifestPromote(args[0])
		},
	}
}
