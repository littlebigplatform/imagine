package version

import (
	"fmt"

	"github.com/spf13/cobra"
)

type Version struct {
	Version string
	Commit  string
	Date    string
}

// NewCmdVersion docs
func NewCmdVersion(v *Version) *cobra.Command {
	c := &cobra.Command{
		Use:                   "version",
		DisableFlagsInUseLine: true,
		Short:                 "display version",
		Long:                  ``,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(v.Version)
			fmt.Println(v.Commit)
			fmt.Println(v.Date)
		},
	}

	return c
}
