package configs

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/spf13/cobra"
	// "github.com/spf13/viper"
	"go.mozilla.org/sops/v3/decrypt"
)

func decryptConfig(target string) {
	fmt.Println("decrypt config form file " + target)
	bytes, _ := ioutil.ReadFile(target)
	yamlMap, err := decrypt.Data(bytes, "yaml")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(yamlMap))
}

// NewCmdPromoteManifest docs
func NewCmdConfigsDecrypt() *cobra.Command {
	return &cobra.Command{
		Use:   "decrypt",
		Short: "decrypt",
		Long:  `decrypt`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			decryptConfig(args[0])
		},
	}
}
