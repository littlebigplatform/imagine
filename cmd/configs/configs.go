package configs

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdManifest docs
func NewCmdConfigs() *cobra.Command {
	c := &cobra.Command{
		Use:   "configs ACTION",
		Short: "configs",
		Long:  `do configs things`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdConfigsDecrypt())

	return c
}
