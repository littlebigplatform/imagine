package list

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	// "github.com/spf13/viper"
)

func manifestList(target string) {
	fmt.Println("list manifests" + target)
}

// NewCmdManifestList docs
func NewCmdManifestList() *cobra.Command {
	return &cobra.Command{
		Use:   "list ITEM",
		Short: "list an item",
		Long:  `Perform a list operation`,
		Run: func(cmd *cobra.Command, args []string) {
			if len(args) == 0 {
				cmd.Help()
				os.Exit(0)
			}

			manifestList(args[0])
		},
	}
}
