package list

import (
	"os"

	"github.com/spf13/cobra"
)

// NewCmdList docs
func NewCmdList() *cobra.Command {
	c := &cobra.Command{
		Use:   "list ACTION",
		Short: "Perform a list operation",
		Long:  `do list things`,
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
			os.Exit(0)
		},
	}

	c.AddCommand(NewCmdManifestList())

	return c
}
