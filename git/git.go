package git

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	gogit "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"

	"gitlab.com/littlebigplatform/imagine/remote"
)

type IRepository interface {
	Path() string
	Remote() remote.Remote
	URL() string
	Author() *object.Signature
	BranchName() (string, error)
	CheckoutMaster() error
	Clone(url string)
	CreateBranch(branchName string) error
	Diff(tag string) (*object.Patch, error)
	InitalCommitHash() string
	LatestTag() (string, error)
	LatestTagContains(tagContains string) (string, error)
	PushWithTags() error
	PushWithTagsTo(remoteName string) error
	Reset(url string)
	Tag(tag string) error
	AddRemote(name string, url string)
}

// Repository represets a Git Repository and remote
type Repository struct {
	path   string
	remote remote.Remote
	url    string
	author *object.Signature
}

// User represents a Users details for authoring commits
type User struct {
	Name  string
	Email string
}

// NewRepositoryOptions are the options required to create a repository
type NewRepositoryOptions struct {
	Path         string
	RemoteType   string
	Organization string
	PAT          string
	Name         string
	User
}

// NewRepository will define a new connection to a repository either existing or to be created.
func NewRepository(opts *NewRepositoryOptions) IRepository {
	return &Repository{
		path:   opts.Path,
		remote: remote.Get(opts.RemoteType, opts.Organization, opts.PAT),
		author: getAuthorDetails(opts.User.Name, opts.User.Email),
	}
}

func (r Repository) Path() string {
	return r.path
}

func (r Repository) Remote() remote.Remote {
	return r.remote
}

func (r Repository) URL() string {
	return r.url
}

func (r Repository) Author() *object.Signature {
	return r.author
}

// AddRemove will add a named remote
func (r Repository) AddRemote(name string, url string) {
	repo, _ := gogit.PlainOpen(r.path)
	repo.CreateRemote(&config.RemoteConfig{
		Name: name,
		URLs: []string{url},
	})
}

// Clone will clone a repository
func (r Repository) Clone(url string) {
	r.url = url
	gogit.PlainClone(r.path, false, &gogit.CloneOptions{
		URL:      r.url,
		Progress: nil,
	})
}

func (r Repository) CheckoutMaster() error {
	repo, _ := gogit.PlainOpen(r.path)
	b := plumbing.ReferenceName("refs/heads/master")

	w, _ := repo.Worktree()
	err := w.Checkout(&gogit.CheckoutOptions{Create: false, Force: false, Branch: b})
	if err != nil {
		return nil
	}
	w.Pull(&gogit.PullOptions{})
	return nil
}

func (r Repository) CreateBranch(branchName string) error {
	repo, _ := gogit.PlainOpen(r.path)
	b := plumbing.ReferenceName("refs/heads/" + branchName)

	w, _ := repo.Worktree()

	err := w.Checkout(&gogit.CheckoutOptions{Create: true, Force: false, Branch: b})
	return err
}

// InitalCommitHash will return the first commit of the repository where not parent commits
// are found.
func (r Repository) InitalCommitHash() string {
	repo, _ := gogit.PlainOpen(r.path)
	commits, _ := repo.CommitObjects()
	var initialHash plumbing.Hash
	_ = commits.ForEach(func(c *object.Commit) error {
		if c.NumParents() == 0 {
			initialHash = c.Hash
		}
		return nil
	})
	return initialHash.String()
}

// BranchName wil return the name of the currently checked out branch.
func (r Repository) BranchName() (string, error) {
	repo, _ := gogit.PlainOpen(r.path)
	branchRefs, err := repo.Branches()
	if err != nil {
		return "", err
	}

	headRef, err := repo.Head()
	if err != nil {
		return "", err
	}

	var currentBranchName string
	err = branchRefs.ForEach(func(branchRef *plumbing.Reference) error {
		if branchRef.Hash() == headRef.Hash() {
			currentBranchName = branchRef.Name().String()

			return nil
		}

		return nil
	})
	if err != nil {
		return "", err
	}

	return currentBranchName, nil
}

// LatestTagContains will return the latest tag that contains the passed string.
func (r Repository) LatestTagContains(tagContains string) (string, error) {
	repo, _ := gogit.PlainOpen(r.path)
	tagRefs, err := repo.Tags()
	if err != nil {
		return "", err
	}

	var latestTagCommit *object.Commit
	var latestTagName string
	err = tagRefs.ForEach(func(tagRef *plumbing.Reference) error {
		if strings.Contains(tagRef.Name().String(), tagContains) {
			revision := plumbing.Revision(tagRef.Name().String())
			tagCommitHash, err := repo.ResolveRevision(revision)
			if err != nil {
				return err
			}

			commit, err := repo.CommitObject(*tagCommitHash)
			if err != nil {
				return err
			}

			if latestTagCommit == nil {
				latestTagCommit = commit
				latestTagName = tagRef.Name().String()
			}

			if commit.Committer.When.After(latestTagCommit.Committer.When) {
				latestTagCommit = commit
				latestTagName = tagRef.Name().String()
			}
		}
		return nil
	})
	if err != nil {
		return "", err
	}

	return latestTagName, nil
}

// LatestTags returns all tags for the most recentagged commit
func (r Repository) LatestTags() ([]string, error) {
	repo, _ := gogit.PlainOpen(r.path)
	tagRefs, err := repo.Tags()
	if err != nil {
		return nil, err
	}

	var latestTagCommit *object.Commit
	var tagNames []string
	err = tagRefs.ForEach(func(tagRef *plumbing.Reference) error {
		revision := plumbing.Revision(tagRef.Name().String())
		tagCommitHash, err := repo.ResolveRevision(revision)
		if err != nil {
			return err
		}

		commit, err := repo.CommitObject(*tagCommitHash)
		if err != nil {
			return err
		}

		if latestTagCommit == nil {
			latestTagCommit = commit
			tagNames = append(tagNames, tagRef.Name().String())
		}

		if commit.Hash == latestTagCommit.Hash {
			tagNames = append(tagNames, tagRef.Name().String())
		} else if commit.Committer.When.After(latestTagCommit.Committer.When) {
			latestTagCommit = commit
			tagNames = make([]string, 0)
		}

		return nil
	})

	if err != nil {
		return nil, err
	}
	return tagNames, nil
}

// LatestTag returns the most recent tag on the repository.
func (r Repository) LatestTag() (string, error) {
	repo, _ := gogit.PlainOpen(r.path)
	tagRefs, err := repo.Tags()
	if err != nil {
		return "", err
	}

	var latestTagCommit *object.Commit
	var latestTagName string
	err = tagRefs.ForEach(func(tagRef *plumbing.Reference) error {
		revision := plumbing.Revision(tagRef.Name().String())
		tagCommitHash, err := repo.ResolveRevision(revision)
		if err != nil {
			return err
		}

		commit, err := repo.CommitObject(*tagCommitHash)
		if err != nil {
			return err
		}

		if latestTagCommit == nil {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		if commit.Committer.When.After(latestTagCommit.Committer.When) {
			latestTagCommit = commit
			latestTagName = tagRef.Name().String()
		}

		return nil
	})
	if err != nil {
		return "", err
	}

	return latestTagName, nil
}

func (r Repository) Tag(tag string) error {
	repo, _ := gogit.PlainOpen(r.path)
	headRef, _ := repo.Head()
	headHash := headRef.Hash()
	_, err := repo.CreateTag(tag, headHash, &gogit.CreateTagOptions{
		Tagger:  r.author,
		Message: tag,
	})
	return err
}

func (r Repository) PushWithTags() error {
	repo, _ := gogit.PlainOpen(r.path)
	rs := config.RefSpec("refs/tags/*:refs/tags/*")
	return repo.Push(&gogit.PushOptions{
		RefSpecs: []config.RefSpec{rs},
	})
}

// PushWithTagsTo pushes everything to a named remote with tags
func (r Repository) PushWithTagsTo(remoteName string) error {
	repo, _ := gogit.PlainOpen(r.path)
	rs := config.RefSpec("refs/tags/*:refs/tags/*")
	return repo.Push(&gogit.PushOptions{
		RefSpecs:   []config.RefSpec{rs},
		RemoteName: remoteName,
	})
}

func (r Repository) Diff(tag string) (*object.Patch, error) {
	repo, _ := gogit.PlainOpen(r.path)
	revision := plumbing.Revision(tag)
	tagCommitHash, err := repo.ResolveRevision(revision)
	if err != nil {
		return nil, err
	}

	tagCommit, err := repo.CommitObject(*tagCommitHash)
	headRef, _ := repo.Head()

	headHash := headRef.Hash()
	headCommit, _ := repo.CommitObject(headHash)
	return tagCommit.Patch(headCommit)
}

// Reset will remove the .git directory and reinitialise and push to origin.
func (r Repository) Reset(url string) {
	r.url = url
	os.RemoveAll(r.path + "/.git")

	repo, _ := gogit.PlainInit(r.path, false)
	repo.CreateRemote(&config.RemoteConfig{
		Name: "origin",
		URLs: []string{r.url},
	})

	w, _ := repo.Worktree()
	w.Add(".")
	w.Commit("Initial commit", &gogit.CommitOptions{
		Author: r.author,
	})
	repo.Push(&gogit.PushOptions{})
}

func getAuthorDetails(name string, email string) *object.Signature {
	username := name
	useremail := email
	if username == "" && useremail == "" {
		cfg, err := parseGitConfig()
		if err != nil {
			log.Fatalln(err)
		}
		username = cfg.Raw.Section("user").Option("name")
		useremail = cfg.Raw.Section("user").Option("email")
	}
	return &object.Signature{
		Name:  username,
		Email: useremail,
		When:  time.Now(),
	}
}

func parseGitConfig() (*config.Config, error) {
	cfg := config.NewConfig()

	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadFile(home + "/.gitconfig")
	if err != nil {
		return nil, err
	}

	if err := cfg.Unmarshal(b); err != nil {
		return nil, err
	}

	return cfg, err
}
