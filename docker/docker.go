package docker

import (
	"bytes"
	"io"
	"io/ioutil"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/client"
	"github.com/docker/docker/pkg/stdcopy"
	"golang.org/x/net/context"
)

type RunOptions struct {
	Image      string
	Datadir    string
	Cmd        []string
	AutoRemove bool
}

type ExecResult struct {
	StdOut   string
	StdErr   string
	ExitCode int
}

func create(ctx context.Context, image string, datadir string, cmd []string) (container.ContainerCreateCreatedBody, error) {
	docker, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer docker.Close()

	var outBuf bytes.Buffer
	reader, err := docker.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return container.ContainerCreateCreatedBody{}, err
	}
	io.Copy(&outBuf, reader)

	resp, err := docker.ContainerCreate(ctx, &container.Config{
		Image: image,
		Cmd:   cmd,
	}, &container.HostConfig{
		Mounts: []mount.Mount{
			mount.Mount{Source: datadir, Target: "/data", Type: "bind"},
		},
	}, nil, "")
	if err != nil {
		return container.ContainerCreateCreatedBody{}, err
	}

	return resp, nil
}

func Run(ctx context.Context, opts *RunOptions) (ExecResult, error) {
	var execResult ExecResult

	cntr, err := create(ctx, opts.Image, opts.Datadir, opts.Cmd)
	if err != nil {
		panic(err)
	}

	docker, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer docker.Close()

	if err := docker.ContainerStart(ctx, cntr.ID, types.ContainerStartOptions{}); err != nil {
		panic(err)
	}

	statusCh, errCh := docker.ContainerWait(ctx, cntr.ID, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			panic(err)
		}
	case <-statusCh:
	}

	var outBuf, errBuf bytes.Buffer
	out, err := docker.ContainerLogs(ctx, cntr.ID, types.ContainerLogsOptions{ShowStdout: true})
	if err != nil {
		panic(err)
	}

	stdcopy.StdCopy(&outBuf, &errBuf, out)

	stdout, err := ioutil.ReadAll(&outBuf)
	if err != nil {
		return execResult, err
	}
	stderr, err := ioutil.ReadAll(&errBuf)
	if err != nil {
		return execResult, err
	}

	res, err := docker.ContainerInspect(ctx, cntr.ID)
	execResult.ExitCode = res.State.ExitCode
	execResult.StdOut = string(stdout)
	execResult.StdErr = string(stderr)

	if opts.AutoRemove {
		err = remove(ctx, cntr.ID)
		if err != nil {
			panic(err)
		}
	}

	return execResult, nil
}

func remove(ctx context.Context, id string) error {
	docker, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		panic(err)
	}
	defer docker.Close()

	return docker.ContainerRemove(ctx, id, types.ContainerRemoveOptions{
		RemoveVolumes: true,
	})
}
