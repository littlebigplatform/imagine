package main

import (
	"fmt"
	"os"

	"gitlab.com/littlebigplatform/imagine/cmd/imagine"
)

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
)

func main() {
	command := imagine.NewCmdImagine(version, commit, date)

	if err := command.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
