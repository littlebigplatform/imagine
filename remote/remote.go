package remote

// Remote is an interface that all remote hosts conform to
type Remote interface {
	Exists(name string) bool
	Create(name string) error
	GetRepoURL(name string) string
}

// Get returns the correct remote host
func Get(name string, org string, pat string) Remote {
	return &gitlabRemote{
		Organization: org,
		PAT:          pat,
	}
}
