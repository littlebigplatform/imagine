package remote

import (
	"context"
	"errors"
	"log"

	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
)

type githubRemote struct {
	Organization string
	PAT          string
}

func (ghr githubRemote) githubClientContext() (*github.Client, context.Context) {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: ghr.PAT},
	)
	tc := oauth2.NewClient(ctx, ts)
	return github.NewClient(tc), ctx
}

func (ghr githubRemote) findGithubRepoByName(name string) (*github.Repository, error) {
	client, ctx := ghr.githubClientContext()
	opt := &github.RepositoryListByOrgOptions{}
	repos, _, _ := client.Repositories.ListByOrg(ctx, "github", opt)
	for _, r := range repos {
		if *r.Name == name {
			return r, nil
		}
	}
	return nil, errors.New("Repository not found")
}

func (ghr githubRemote) Exists(name string) bool {
	_, err := ghr.findGithubRepoByName(name)
	if err != nil {
		return false
	}
	return true
}

func (ghr githubRemote) GetRepoURL(name string) string {
	r, err := ghr.findGithubRepoByName(name)
	if err != nil {
		log.Fatalln(err)
	}
	return *r.SSHURL
}

func (ghr githubRemote) Create(name string) error {
	client, ctx := ghr.githubClientContext()
	ghorg, _, _ := client.Organizations.Get(ctx, ghr.Organization)
	r := &github.Repository{
		Name:         github.String(name),
		Organization: ghorg,
		Private:      github.Bool(true),
		Description:  github.String("just a test repo"),
	}
	_, _, err := client.Repositories.Create(ctx, "Scaleware", r)
	return err
}
