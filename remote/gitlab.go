package remote

import (
	"errors"
	"log"

	"github.com/xanzy/go-gitlab"
)

type gitlabRemote struct {
	Client       *gitlab.Client
	Organization string
	PAT          string
}

func (glr gitlabRemote) findProjectByName(name string) (*gitlab.Project, error) {
	if glr.Client == nil {
		glr.Client = gitlab.NewClient(nil, glr.PAT)
	}

	p := &gitlab.ListProjectsOptions{
		OrderBy:    gitlab.String("name"),
		Sort:       gitlab.String("desc"),
		Simple:     gitlab.Bool(true),
		Search:     gitlab.String(name),
		Membership: gitlab.Bool(true),
	}
	projects, _, err := glr.Client.Projects.ListProjects(p)

	if err != nil {
		log.Fatalf("error communicating with gitlab")
	}

	for _, project := range projects {
		if project.PathWithNamespace == glr.Organization+"/"+name {
			return project, nil
		}
	}

	return nil, errors.New("Project not found")
}

func (glr gitlabRemote) Exists(name string) bool {
	_, err := glr.findProjectByName(name)
	if err != nil {
		return false
	}
	return true
}

func (glr gitlabRemote) GetRepoURL(name string) string {
	p, err := glr.findProjectByName(name)
	if err != nil {
		log.Fatalln(err)
	}
	return p.SSHURLToRepo
}

func (glr gitlabRemote) Create(name string) error {
	glab := gitlab.NewClient(nil, glr.PAT)
	g, _, _ := glab.Groups.SearchGroup(glr.Organization)
	p := &gitlab.CreateProjectOptions{
		Name:                 gitlab.String(name),
		NamespaceID:          gitlab.Int(g[0].ID),
		Description:          gitlab.String("Just a test project to play with"),
		MergeRequestsEnabled: gitlab.Bool(true),
		SnippetsEnabled:      gitlab.Bool(false),
		Visibility:           gitlab.Visibility(gitlab.PrivateVisibility),
		WikiEnabled:          gitlab.Bool(false),
	}
	_, _, err := glab.Projects.CreateProject(p)
	return err
}
