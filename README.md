# Imagine

A tool to develop and create a `Little Big Platform`

# Developing

Clone the repo then run `go mod download`

To run `go run main.go`

# Tests

TBD

mockgen -source=git/git.go -destination git/mock_git/mock_git.go
